package com.springmars.journey.caffeine.entity;

import lombok.Data;

/**
 * @author pf ()
 * @date 2021/8/31 13:15
 */
@Data
public class ResponseData {

    private int code;
    private String error;
    private Object data;


    private ResponseData(int code, String error) {
        this.code = code;
        this.error = error;
    }

    private ResponseData(int code, Object data) {
        this.code = code;
        this.data = data;
    }

    public static ResponseData success(Object data){
        return new ResponseData(200,data);
    }

    public static ResponseData error(String msg){
        return new ResponseData(100,msg);
    }
}
