package com.springmars.journey.caffeine.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author pf ()
 * @date 2021/8/31 10:51
 */
@Data
@NoArgsConstructor
public class Student {
    private int id;
    private String name;

    public Student(int id) {
        this.id = id;
    }

    public Student(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
