package com.springmars.journey.caffeine.flow;

import com.github.benmanes.caffeine.cache.AsyncCache;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.springmars.journey.caffeine.entity.Student;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * @author pf (Caffeine提供了四种缓存添加策略：手动加载，自动加载，手动异步加载和自动异步加载。)
 * @date 2021/8/31 10:50
 */
public class Caffeine1 {

    /**
     * 手动加载
     */
    public void population1() {
        Cache<Integer, Student> cache = Caffeine.newBuilder()
                .expireAfterWrite(10, TimeUnit.MINUTES)
                .maximumSize(100).build();
        // 查找一个缓存元素， 没有查找到的时候返回null
        Student s1 = cache.getIfPresent(1);
        // 查找缓存，如果缓存不存在则生成缓存元素,  如果无法生成则返回null
        cache.get(2, Student::new);
        // 添加或者更新一个缓存元素
        cache.put(3, new Student());
        //  移除一个缓存元素
        cache.invalidate(3);
    }

    /**
     * 自动加载
     */
    public void population2() {
        LoadingCache<Integer, Student> loadingCache = Caffeine.newBuilder()
                .expireAfterWrite(10, TimeUnit.MINUTES)
                .maximumSize(100)
                // build 提供获取失败则手动加载方法
                .build(k -> {
                    k++;
                    return new Student(k);
                });
        // 查找缓存，如果缓存不存在则生成缓存元素,  如果无法生成则返回null
        Student s = loadingCache.get(1);
        // 批量查找缓存，如果缓存不存在则生成缓存元素
        Map<Integer, Student> ss = loadingCache.getAll(Arrays.asList(1,2,3));
    }

    /**
     * 手动异步加载
     */
    public void population3() {
        AsyncCache<Integer, Student> asyncCache = Caffeine.newBuilder()
                .expireAfterWrite(10, TimeUnit.MINUTES)
                .maximumSize(100)
                .buildAsync();

        // 查找一个缓存元素， 没有查找到的时候返回null
        CompletableFuture<Student> future = asyncCache.getIfPresent(1);
        if(future != null){
            future.whenComplete((s, e)->{
                if (s != null) {
                    // do something
                }
            });
        }
        asyncCache.get(1, (Function<Integer, Student>) Student::new);
        // 添加或者更新一个缓存元素
        asyncCache.put(2,CompletableFuture.supplyAsync(Student::new));
        // 移除一个缓存元素
        asyncCache.synchronous().invalidate(3);

    }

}
