package com.springmars.journey.caffeine.web;

/**
 * @author pf ()
 * @date 2021/8/31 12:04
 */
public enum AppCache {

    /** 缓存对象 */
    Student(100),

    ;

    private static final int DEFAULT_MAX_SIZE = 1000;
    private static final int DEFAULT_TTL = 10;

    private int size;
    private int ttl;

    AppCache(int size) {
        this.size = size;
        this.ttl = DEFAULT_TTL;
    }

    AppCache(int size, int ttl) {
        this.size = size;
        this.ttl = ttl;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getTtl() {
        return ttl;
    }

    public void setTtl(int ttl) {
        this.ttl = ttl;
    }

}
