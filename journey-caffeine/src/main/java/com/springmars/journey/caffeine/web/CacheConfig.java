package com.springmars.journey.caffeine.web;

import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author pf ()
 * @date 2021/8/31 11:51
 */
@Configuration
public class CacheConfig {

    @Bean
    @Primary
    public CacheManager caffeineCacheManager(){
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        AppCache[] appCaches = AppCache.values();
        List<CaffeineCache> caffeineCaches = new ArrayList<>(appCaches.length);
        // build caffeine cache
        for (AppCache appCache : appCaches) {
            caffeineCaches.add(
                    new CaffeineCache(
                            appCache.name(),
                            Caffeine.newBuilder().recordStats()
                                    .expireAfterWrite(appCache.getTtl(), TimeUnit.MINUTES)
                                    .maximumSize(appCache.getSize())
                                    .build()
                    )
            );
        }
        // set cache
        cacheManager.setCaches(caffeineCaches);
        return cacheManager;
    }
}
