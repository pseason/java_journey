package com.springmars.journey.caffeine.web;

import com.springmars.journey.caffeine.entity.ResponseData;
import com.springmars.journey.caffeine.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.*;

/**
 * @author pf ()
 * @date 2021/8/31 11:30
 */
@RestController
@RequestMapping("student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private CacheManager caffeineCacheManager;

    @GetMapping("get/{id}")
    public ResponseData getStudent(@PathVariable("id") int id){
        return ResponseData.success(studentService.getStudent(id));
    }

    @GetMapping("update")
    public ResponseData updateStudent(@RequestParam("id") int id,@RequestParam("name") String name){
        return ResponseData.success(studentService.putStudent(new Student(id,name)));
    }

    @GetMapping("delete/{id}")
    public ResponseData deleteStudent(@PathVariable("id") int id){
        return ResponseData.success(studentService.deleteById(id));
    }

}
