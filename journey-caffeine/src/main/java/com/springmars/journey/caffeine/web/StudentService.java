package com.springmars.journey.caffeine.web;

import com.springmars.journey.caffeine.entity.Student;
import lombok.extern.log4j.Log4j2;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * @author pf ()
 * @date 2021/8/31 11:30
 */
@Log4j2
@Service
@CacheConfig(cacheManager = "caffeineCacheManager",cacheNames = "Student")
public class StudentService {

    private Map<Integer, Student> studentMap = new HashMap<>();

    @PostConstruct
    public void load(){
        studentMap.put(1,new Student(1,"lucy"));
        studentMap.put(2,new Student(2,"lili"));
        studentMap.put(3,new Student(3,"jack"));
        studentMap.put(4,new Student(4,"joe"));
    }

    @Cacheable(key = "#id",unless = "#result == null")
    public Student getStudent(Integer id){
        log.info("get Student by id: {}",id);
        return studentMap.get(id);
    }

    @CachePut(key = "#student.id")
    public Student putStudent(Student student){
        return studentMap.put(student.getId(),student);
    }

    @CacheEvict(key = "#id")
    public Student deleteById(Integer id) {
        return studentMap.remove(id);
    }


}
