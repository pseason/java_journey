package com.springmars.journey.aggregation;

import com.microsoft.gctoolkit.GCToolKit;
import com.microsoft.gctoolkit.io.GCLogFile;
import com.microsoft.gctoolkit.io.SingleGCLogFile;
import com.microsoft.gctoolkit.jvm.JavaVirtualMachine;

import java.nio.file.Path;

public class Main {

    private final static String gcLogFilePath = "C:\\Users\\pengshuo\\Desktop\\parallelgc.log";

    public static void main(String[] args) throws Exception {
        execute();
    }

    public static void execute() {
        GCLogFile logFile = new SingleGCLogFile(Path.of(gcLogFilePath));
        GCToolKit gcToolKit = new GCToolKit();
        JavaVirtualMachine machine = gcToolKit.analyze(logFile);
        HeapOccupancyAfterCollectionSummary results = machine.getAggregation(HeapOccupancyAfterCollectionSummary.class);
        results.get().forEach((gcType, dataSet) ->
            System.out.println("The XYDataSet for " + gcType + " contains " + dataSet.size() + " items.")
        );
    }
}
